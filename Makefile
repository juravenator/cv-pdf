SHELL:=/bin/bash
.DEFAULT_GOAL := help

##
### Main commands
##

.PHONY: pdf
pdf: cv.pdf cover_letter.pdf ## generate PDF files
cv.pdf: cv
	./makepdf.sh cv
cover_letter.pdf: cover_letter
	./makepdf.sh cover_letter

.PHONY: cv
cv: build/cv.html ## generate CV HTML
build/cv.html: qr node_modules $(shell find src -type f | sed 's|\s|\\ |g')
	cd src && go run generate.go cv && cd ..
	npx webpack --config webpack.config.prod.js

.PHONY: cover_letter
cover_letter: build/cover_letter.html ## generate cover letter HTML
build/cover_letter.html: qr node_modules $(shell find src -type f | sed 's|\s|\\ |g')
	cd src && go run generate.go cover_letter && cd ..
	npx webpack --config webpack.config.prod.js

##
### Helper commands
##

.PHONY: lint
lint: ## lint code
	npx stylelint --fix "src/**/*.scss"

.PHONY: qr
qr: src/img/qr.png ## generate a new QR code
src/img/qr.png:
	mkdir -p $(dir $@)
	qrencode -o $@ -s1 -lM -m0 --foreground=FFFFFFFF --background=00000000 https://juravenator.dev

node_modules: package.json
	npm install --unsafe-perm
	touch node_modules

.PHONY: clean
clean: ## clean repository
	rm -rf build cv.pdf cover_letter.pdf src/cv.html src/cover_letter.pdf

include .makefile/dockerized.mk
include .makefile/help.mk