FROM justinribeiro/chrome-headless
LABEL maintainer="Glenn Dirkx <glenn.dirkx@gmail.com>"

USER 0
RUN apt-get update && \
    apt-get install -y qrencode make python3 curl ghostscript git && \
    rm -rf /var/lib/apt/lists/*

RUN groupadd -g 1000 docker && useradd -u 1000 -g docker -s /bin/bash docker && \
    mkdir -p /home/docker && chown docker:docker /home/docker

# NodeJS
ARG NODE_VERSION=14.16.1
RUN curl -o node.tar.xz https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz && \
    mkdir -p /usr/local/lib/nodejs && tar -xJvf node.tar.xz -C /usr/local/lib/nodejs && rm node.tar.xz
ENV PATH="$PATH:/usr/local/lib/nodejs/node-v${NODE_VERSION}-linux-x64/bin"

# Golang
ARG GO_VERSION=1.16.4
ENV PATH="$PATH:/usr/local/go/bin"
RUN curl -o go.tar.gz https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go.tar.gz && rm -f go.tar.gz

USER docker
SHELL ["/bin/bash", "-c"]